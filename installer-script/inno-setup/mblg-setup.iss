[Setup]
AppName=Moosburger Basar Liste Generator
AppVersion=2.0
AppCopyright=2017-2019 Jason Reed
AppId={{F5FFF1FA-5776-4C72-8907-B203E08FB708}
AppPublisher=Jason Reed
AppPublisherURL=https://jrsofty1.stinkbugonline.com
AppSupportURL=https://bitbucket.org/jrsofty/moosburger-basar-list-generator/issues
AppContact=jrsofty@gmail.com
VersionInfoVersion=1.0.0
VersionInfoCompany=JRSofty Programming
VersionInfoDescription=Moosburger Basar Liste Generator
VersionInfoTextVersion=2.0
VersionInfoCopyright=2017-2019 Jason Reed
VersionInfoProductName=MBLG
VersionInfoProductVersion=2.0.0
VersionInfoProductTextVersion=2.0
OutputDir=..\..\target\installer
OutputBaseFilename=MBLG-Installer
SolidCompression=True
DefaultDirName={pf}\JRSofty\mblg
LanguageDetectionMethod=locale
SetupIconFile=..\..\target\deploy\mblg.ico
UninstallDisplayName=Moosburg Basar Liste Generator
UninstallDisplayIcon={app}\mblg.ico

[Files]
Source: "..\..\target\deploy\mblg.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\target\deploy\licenses\LICENSE.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\target\deploy\README.MD"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\target\deploy\licenses\LICENSE_DE.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\target\deploy\mblg.ico"; DestDir: "{app}"

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"; LicenseFile: "..\..\target\deploy\licenses\LICENSE.txt"
Name: "german"; MessagesFile: "compiler:Languages\German.isl"; LicenseFile: "..\..\target\deploy\licenses\LICENSE_DE.txt" 

[Registry]
Root: "HKLM"; Subkey: "SOFTWARE\JRSofty\mblg"; ValueType: string; ValueName: "Version"; ValueData: "2.0.0"; Flags: createvalueifdoesntexist uninsdeletekeyifempty uninsdeletevalue

[Icons]
Name: "{group}\Moosburger Basar Liste Generator"; Filename: "{app}\mblg.exe"; WorkingDir: "{userdocs}"; IconFilename: "{app}\mblg.ico"; IconIndex: 0; Languages: english german
Name: "{userdesktop}\Moosburger Basar Liste Generator"; Filename: "{app}\mblg.exe"; WorkingDir: "{userdocs}"; IconFilename: "{app}\mblg.ico"; IconIndex: 0; Languages: english german

[ThirdParty]
UseRelativePaths=True
