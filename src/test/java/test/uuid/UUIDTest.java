package test.uuid;
import java.util.UUID;

import org.bitbucket.jrsofty.mblg.util.UUIDHelper;
import org.junit.Assert;
import org.junit.Test;

public class UUIDTest {

    private final String source = "3f76fc4a-8105-4099-8446-01a9732ace71";
    private final String stripped = "3f76fc4a81054099844601a9732ace71";

    @Test
    public void test() {
        final UUID normalUuid = UUID.fromString(this.source);
        final UUIDHelper helper = new UUIDHelper();
        final UUID builtUuid = helper.fromMalformedString(this.stripped);
        Assert.assertEquals(normalUuid, builtUuid);
    }

}
