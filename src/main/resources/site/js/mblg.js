const KINDER_MAX_ITEMS = 50;
const MODE_MAX_ITEMS = 30;
const VOLUNTEER_KINDER_MAX_ITEMS = 100;
const VOLUNTEER_MODE_MAX_ITEMS = 100;
const MAX_VOLUNTEER_ID = 600;

var activeTabUuid = null;
var tabCount = 0;

function getId() {
	tabCount++;
	return tabCount;
}

(function($) {
	$(document).ready(function() {
		// Event Controllers
		
		$.getJSON('rest/list/all', function(result) {
			$.each(result, function(ndx, itm) {
				$.buildTabPanelFromResult(itm);
			});
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Load Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
		
		$.getJSON('rest/control/version', function(result){
			$('#version').text(result.version);
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Load Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
		
		$('#openSettingsDialog').on('click', function() {
			$.getJSON("rest/control/settings/get", function(result){
				 $("#defaultFirstName").val(result.defaultFirstName);
				 $("#defaultLastName").val(result.defaultLastName);
				 $("#defaultBasarNumber").val(result.defaultBasarNumber);
				 $("#servicePort").val(result.servicePort);
				 $("option[value = '" + result.defaultLang + "']").attr("selected", true);
				 if(result.autoReport){
					 $("#autoReport").attr("checked", true);
				 }else{
					 $("#autoReport").removeAttr("checked");
				 }
				 
				 if(result.autoUpdate){
					 $("#autoUpdate").attr("checked", true);
				 }else{
					 $("#autoUpdate").removeAttr("checked");
				 }
				 
				 if(result.enableDebug){
					 $("#enableDebug").attr("checked",true);
				 }else{
					 $("#enableDebug").removeAttr("checked");
				 }
				
				$('#settingsDialog').modal('show');
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("Settings Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});;
			
		});
		
		$("#saveSettings").on('click', function(){
			$("#servicePort").removeClass("border").removeClass("border-danger");
			$("#defaultBasarNumber").removeClass("border").removeClass("border-danger");
			var settings = new Object();
			var basarNumber = $("#defaultBasarNumber").val();
			var port = $("#servicePort").val();
			
			if(isNaN(port) || port === true || port === ''){
				$("#servicePort").addClass("border").addClass('border-danger');
				return null;
			}
			
			if(basarNumber !== '' && (isNaN(basarNumber) || basarNumber === true || basarNumber > 999 || basarNumber < 400)){
				$("#defaultBasarNumber").addClass("border").addClass('border-danger');
				return null;
			}
			
			settings.defaultFirstName = $("#defaultFirstName").val();
			settings.defaultLastName = $("#defaultLastName").val();
			settings.defaultBasarNumber = basarNumber;
			settings.servicePort = port;
			settings.defaultLang = $("option[selected]").val();
			
			settings.autoUpdate = $.hasAttr($("#autoUpdate").attr("checked"));
			settings.autoReport = $.hasAttr($("#autoReport").attr("checked"));
			settings.enableDebug = $.hasAttr($("#enableDebug").attr("checked"));
			
			$.postJSON("rest/control/settings/save", settings, function(result){
				$("#settingsDialog").modal("hide");
			});
			
		});
		
		$('#closeListFile').on('click',function(){
			$.getJSON("rest/list/" + activeTabUuid + "/isDirty", function(result) {
				if(result.dirty){
					return;
				}
				
				$.getJSON("rest/list/closeList/" + activeTabUuid, function(clsResult){
					$("li.nav-item[data-uuid = '" + activeTabUuid + "']").remove();
					$("div.tab-pane[data-uuid = '" + activeTabUuid + "']").remove();
					$("a.list-link").first().trigger('click');
				});
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("Load Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});
		});
		
		$('#newListFile').on('click', function() {
			$.createNewList();
		});
		
		$('#openListFile').on('click', function() {
			$.getJSON('rest/control/openFileDialog', function(result) {
				if(result.filepath == ''){
					return;
				}
				$.postJSON("rest/list/load", result, function(data) {
					$.buildTabPanelFromResult(data);
				});
				
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("Load Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});
		});
		
		$('#newTabLink').on('click', function() {
			$.createNewList();
		});
		
		$('#saveList').on('click', function(){
			$.saveList();
		});
		
		$('#saveListAs').on('click',function(){
			$.saveListAs();
		});
		
		$('#exitApplication').on('click',function(){
			
		});
		
		$('#printLabels').on('click', function(){
			$.printLabels();
		});
		
		$('#printAllPages').on('click',function(){
			$.printAll();
		});
		
		$('#printList').on('click',function(){
			$.printList();
		});
		
		$('#printNumber').on('click', function(){
			$.printNumber();
		});
		
		$('#printEmpty').on('click',function(){
			$.printEmpty();
		});
		
		$('#newListButton').on('click', function(){
			$.createNewList();
		});
		
		$('#saveListButton').on('click', function(){
			$.saveList();
		});
		
		$('#saveListAsButton').on('click', function(){
			$.saveListAs();
		});
		
		$('#printAllButton').on('click',function(){
			$.printAll();
		});
		
		$('#printListButton').on('click',function(){
			$.printList();
		});
		
		$('#printLabelsButton').on('click', function(){
			$.printLabels();
		});
		
		$('#aboutMblg').on('click', function(){
			$("#aboutDialog").modal('show');
		});
		
		$("#saveItem").on('click', function() {
			// validate form
			var item = $.validateItemAndReturn();
			if(item === null){
				return;
			}
			
			
			var $restUrl = "rest/list/" + item.listId + "/add";
			var modify = false;
			
			if (typeof item.internalId !== typeof undefined && item.internalId !== false && item.internalId !== '') {
				$restUrl = "rest/list/" + item.listId + "/modify/" + item.internalId;
				modify = true;
			}
			
			$.postJSON($restUrl, item, function(result) {
				var $table = $("table[data-uuid = '" + item.listId + "']");
				if (!modify) {
					$.createItemRow($table, result, item.listId);
				} else {
					$.updateItemRow(result);
				}
				$.cleanItemDialog();
				$("#itemEntryDialog").modal("hide");
				$.isListDirty(item.listId);
			});
			
		});
		
		$("#saveAndNew").on('click', function(){
			var item = $.validateItemAndReturn();
			if(item === null){
				return;
			}
			
			var $restUrl = "rest/list/" + item.listId + "/add";
			var modify = false;
			
			if (typeof item.internalId !== typeof undefined && item.internalId !== false && item.internalId !== '') {
				$restUrl = "rest/list/" + item.listId + "/modify/" + item.internalId;
				modify = true;
			}
			
			$.postJSON($restUrl, item, function(result) {
				var $table = $("table[data-uuid = '" + item.listId + "']");
				if (!modify) {
					$.createItemRow($table, result, item.listId);
				} else {
					$.updateItemRow(result);
				}
				$.cleanItemDialog();
				
				$.isListDirty(item.listId);
			});
		});
		
	});
	
	$.removeInputError = function(){
		$("input[name='description']").removeClass("border").removeClass("border-danger");
		$("input[name='price']").removeClass("border").removeClass('border-danger');
	}
	
	$.validateItemAndReturn = function(){
		var $listId = $("input[name='listUUID']").val();
		var $itemId = $("input[name='itemUUID']").val();
		var $itemNum = $("input[name='itemNumber']").val();
		var $desc = $("input[name='description']").val();
		var $size = $("input[name='size']").val();
		var $price = $("input[name='price']").val();
		
		if(isNaN($price) || $price === true || $price === ''){
			$("input[name='price']").addClass("border").addClass('border-danger');
			return null;
		}
		if ($desc === "") {
			$("input[name='description']").addClass("border").addClass('border-danger');
			return null;
		}
		
		
		var item = new Object();
		item.internalId = $itemId;
		item.description = $desc;
		item.size = $size;
		item.price = $price.replace('.', ',');
		item.number = $itemNum;
		item.listId = $listId;
		
		return item;
	}
	
	
	$.createNewList = function(){
		$.getJSON('rest/list/new', function(result) {
			$.buildTabPanelFromResult(result);
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("New List Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	$.saveList = function(){
		$.getJSON('rest/list/save/' + activeTabUuid, function(result){
			if(result.hasOwnProperty("status")){
				$.saveListAs();
				return;
			}
			$("a.list-link[data-uuid='" + result.id + "']").text(data.meta.listName);
			$.isListDirty(result.id);
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Save List Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	$.saveListAs = function(){
		$.getJSON('rest/control/saveFileDialog', function(result){
			if(result.filepath == ''){
				return;
			}
			$.postJSON('rest/list/saveAs/' + activeTabUuid , result, function(data){
				$("a.list-link[data-uuid = '" + data.id + "']").text(data.meta.listName);
				$.isListDirty(data.id);
			});
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Save List As Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	$.hasAttr = function(attr){
		if(typeof attr !== typeof undefined && attr !== false){
			return true;
		}
		return false;
	}
	
	$.printAll = function(){
		if(activeTabUuid === null){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text("No tab selected.");
			$("#alertToast").toast('show');
			return;
		}
		$.getJSON('rest/control/printAll/' + activeTabUuid, function(){
			console.log("Print job started");
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	$.printList = function(){
		if(activeTabUuid === null){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text("No tab selected.");
			$("#alertToast").toast('show');
			return;
		}
		$.getJSON('rest/control/printList/' + activeTabUuid, function(){
			console.log("Print job started");
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	$.printLabels = function(){
		if(activeTabUuid === null){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text("No tab selected.");
			$("#alertToast").toast('show');
			return;
		}
		$.getJSON('rest/control/printLabels/' + activeTabUuid, function(){
			console.log("Print job started");
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	$.printNumber = function(){
		if(activeTabUuid === null){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text("No tab selected.");
			$("#alertToast").toast('show');
			return;
		}
		$.getJSON('rest/control/printNumber/' + activeTabUuid, function(){
			console.log("Print job started");
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	$.printEmpty = function(){
		if(activeTabUuid === null){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text("No tab selected.");
			$("#alertToast").toast('show');
			return;
		}
		$.getJSON('rest/control/printEmpty', function(){
			console.log("Print job started");
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Print Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}
	
	// OPERATIONAL FUNCTIONS
	
	$.postJSON = function(url, data, callback) {
		return jQuery.ajax({
		    headers : {
		        'Accept' : 'application/json',
		        'Content-Type' : 'application/json'
		    },
		    'type' : 'POST',
		    'url' : url,
		    'data' : JSON.stringify(data),
		    'dataType' : 'json',
		    'success' : callback
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("Data Transfer Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	};
	
	$.cleanItemDialog = function() {
		$("input[name='listUUID']").val("");
		$("input[name='itemUUID']").val("");
		$("input[name='itemNumber']").val("");
		$("input[name='description']").val("");
		$("input[name='size']").val("");
		$("input[name='price']").val("");
	}

	$.buildTabPanelFromResult = function(result) {
		var localId = getId();
		var tabId = "tab-" + localId;
		var conId = "cnt-" + localId;
		var $navItemWrapper = $("#nav-item-template").clone();
		$navItemWrapper.removeAttr("id");
		$navItemWrapper.removeClass("hidden");
		$navItemWrapper.attr("data-uuid", result.id);
		
		var $navItemLink = $navItemWrapper.find("a.list-link");
		$navItemLink.text(result.meta.listName);
		$navItemLink.attr("id", tabId);
		$navItemLink.attr("data-tabId", localId);
		$navItemLink.attr("data-uuid", result.id);
		$navItemLink.attr("data-target", "#" + conId);
		$("#tab-nav-list .nav-item:last").before($navItemWrapper);
		
		var $tabPanel = $("#content-1-template").clone();
		$tabPanel.attr("id", conId);
		$tabPanel.attr("data-uuid", result.id);
		$tabPanel.attr("data-tabId", localId);
		$tabPanel.find("[data-uuid]").each(function(ndx, item) {
			var origId = $(item).attr("id");
			$(item).attr("data-uuid", result.id);
			$(item).attr("id", origId + "-" + localId);
			if ($(item).is("input")) {
				var origName = $(item).attr("name");
				if (typeof origName !== typeof undefined && origName !== false) {
					$(item).attr("name", origName + "-" + localId);
				}
			}
			if ($(item).is("label")) {
				var origFor = $(item).attr("for");
				$(item).attr("for", origFor + "-" + localId);
			}
		});
		
		$tabPanel.find("input[type='radio'][value='" + result.header.listType + "']").prop("checked", true);
		$tabPanel.find("input[type='radio'][data-uuid='" + result.id + "']").on("change", function() {
			$.getJSON("rest/list/" + result.id + "/setType/" + this.value, function() {
				$.isListDirty(result.id);
			});
		});
		$tabPanel.find("#basarNumber-" + localId).val(result.header.customerNumber).on("change paste keyup", function() {
			
			if (this.value == '') {
				return;
			}
			if (this.value.length > 3) {
				this.value = this.value.substring(0, 3);
				console.log("Trimmed too long");
			}
			if (this.value.length < 3) {
				return;
			}
			if (isNaN(this.value)) {
				this.value = '';
				console.log("No non-numerics please.");
				return;
			}
			if (this.value.length == 3 && (this.value < 400 || this.value > 999)) {
				console.log("Invalid entry: " + this.value);
				this.value = '';
				return;
			}
			$.getJSON("rest/list/" + result.id + "/setCustomerNumber/" + this.value, function() {
				$.isListDirty(result.id);
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("List Property Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});;
		});
		$tabPanel.find("#firstName-" + localId).val(result.header.firstName).on("change paste keyup", function() {
			
			$.getJSON("rest/list/" + result.id + "/setFirstName/" + encodeURIComponent(this.value), function() {
				$.isListDirty(result.id);
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("List Property Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});;
		});
		$tabPanel.find("#lastName-" + localId).val(result.header.lastName).on("change paste keyup", function() {
			$.getJSON("rest/list/" + result.id + "/setLastName/" + encodeURIComponent(this.value), function() {
				console.log("Changed value in backend.");
				$.isListDirty(result.id);
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("List Property Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});;
		});
		$tabPanel.find("#container-" + localId).val(result.header.container).on("change paste keyup", function() {
			$.getJSON("rest/list/" + result.id + "/setContainer/" + encodeURIComponent(this.value), function() {
				console.log("Changed value in backend.");
				$.isListDirty(result.id);
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("List Property Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});;
		});
		if (typeof result.header.container != typeof undefined && result.header.container !== false) {
			$tabPanel.find("#container-" + localId).val(result.header.container);
		}
		
		var $table = $tabPanel.find("#listTbl-" + localId);
		
		$.each(result.body.items, function(k, v) {
			$.createItemRow($table, v, result.id);
		});
		
		$("#tabContent").append($tabPanel);
		$(document).on("click", "a.list-link", function(e) {
			e.preventDefault();
			var $lnk = $(this);
			activeTabUuid = $lnk.attr("data-uuid");
			$(this).tab("show");
		});
		
		$(document).on("click", "a.editLink", function(e) {
			e.preventDefault();
			var $lnk = $(this);
			var $lst = $lnk.attr("data-uuid");
			var $itm = $lnk.attr("data-item");
			$.displayItemDialogWithItem($lst, $itm);
		});
		$(document).on("click", "a.delLink", function(e) {
			e.preventDefault();
			var $lnk = $(this);
			$.getJSON("rest/list/" + result.id + "/remove/" + $lnk.attr("data-item"), function(result) {
				$("tr[data-item = '" + $lnk.attr("data-item") + "']").remove();
				
			}).fail(function(jqxhr, textStatus, error){
				$("#toastHeaderText").text("List Item Failure");
				$("#toastContent").text(error);
				$("#alertToast").toast('show');
			});;
		});
		$(document).on("click", "a.moveUp", function(e) {
			e.preventDefault();
			var $lnk = $(this);
		});
		$(document).on("click", "a.moveDwn", function(e) {
			e.preventDefault();
			var $lnk = $(this);
		});
		$(document).on("click", "button.btn-new-item", function(e) {
			e.preventDefault();
			
			var $btn = $(this);
			$("input[name='listUUID']").val($btn.attr("data-uuid"));
			$("#itemEntryDialog").modal("show");
		});
		
		$navItemLink.trigger('click');
		$.isListDirty(result.id);
	}

	$.validateListItemCount = function(listId, callback) {
		$.getJSON("rest/list/" + listId + "/count", function(result) {
			if (result.custNo < MAX_VOLUNTEER_ID) {
				if (result.listType == "KINDERKLEIDER") {
					if (result.items < VOLUNTEER_KINDER_MAX_ITEMS) {
						callback();
					}
				} else {
					if (result.items < VOLUNTEER_MODE_MAX_ITEMS) {
						callback();
					}
				}
			} else {
				if (result.listType == "KINDERKLEIDER") {
					if (result.items < KINDER_MAX_ITEMS) {
						callback();
					}
				} else {
					if (result.items < MODE_MAX_ITEMS) {
						callback();
					}
				}
			}
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("List Validation Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});;
	};
	
	$.updateItemRow = function(data) {
		var $numColumn = $("td[data-header='number'][data-item='" + data.internalId + "']");
		var $descColumn = $("td[data-header='desc'][data-item='" + data.internalId + "']");
		var $sizeColumn = $("td[data-header='size'][data-item='" + data.internalId + "']");
		var $priceColumn = $("td[data-header='price'][data-item='" + data.internalId + "']");
		
		$numColumn.text(data.number);
		$descColumn.text(data.description);
		$sizeColumn.text(data.size);
		$priceColumn.text(data.price);
		
		
	}

	$.createItemRow = function(table, data, listId) {
		var $row = $("<tr></tr>").attr("data-item", data.internalId);
		$("<td data-header='number' data-item='" + data.internalId + "'>" + data.number + "</td>").appendTo($row);
		$("<td data-header='desc' data-item='" + data.internalId + "'>" + data.description + "</td>").appendTo($row);
		$("<td data-header='size' data-item='" + data.internalId + "'>" + data.size + "</td>").appendTo($row);
		$("<td data-header='price' data-item='" + data.internalId + "'>" + data.price + "</td>").appendTo($row);
		var $ctrl = $("<td></td>");
		$("<a href='#' class='item-button border rounded editLink' data-uuid='" + listId + "' data-item='" + data.internalId + "'><i class='fas fa-pencil-alt'></i></a>").appendTo(
		        $ctrl);
		$("<a href='#' class='item-button border rounded delLink' data-uuid='" + listId + "' data-item='" + data.internalId + "'><i class='fas fa-trash-alt'></i></a>").appendTo(
		        $ctrl);
		// $('<a href="#" class="item-button border rounded moveUp" data-uuid="'
		// + listId + '" data-item="' + data.internalId +'"><i class="fas
		// fa-angle-double-up"></i></a>').appendTo($ctrl);
		// $('<a href="#" class="item-button border rounded moveDwn"
		// data-uuid="' + listId + '" data-item="' + data.internalId +'"><i
		// class="fas fa-angle-double-down"></i></a></td>').appendTo($ctrl);
		$ctrl.appendTo($row);
		$row.appendTo(table);
	}

	$.displayItemDialogWithItem = function(listId, itemId) {
		var url = "rest/list/get/" + listId + "/item/" + itemId;
		$.getJSON(url, function(result) {
			
			$("#listId").val(listId);
			$("#itemId").val(itemId);
			$("#itemNum").val(result.number);
			$("#description").val(result.description);
			$("#size").val(result.size);
			$("#price").val(result.price);
			$("#itemEntryDialog").modal("show");
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("List Item Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});
	}

	$.isListDirty = function(listId) {
		$.getJSON("rest/list/" + listId + "/isDirty", function(result) {
			var $lnk = $("a.list-link[data-uuid = '" + listId + "']").text();
			if (result.dirty && $lnk.substring(0, 1) != '*') {
				$("a.list-link[data-uuid = '" + listId + "']").text("*" + $lnk);
			} else if (!result.dirty) {
				if ($lnk.substring(0, 1) === '*') {
					$("a.list-link[data-uuid = '" + listId + "']").text($lnk.slice(1));
				}
			}
		}).fail(function(jqxhr, textStatus, error){
			$("#toastHeaderText").text("List Property Failure");
			$("#toastContent").text(error);
			$("#alertToast").toast('show');
		});;
	}

}(jQuery));
