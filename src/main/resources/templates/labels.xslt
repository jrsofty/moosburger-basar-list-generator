<?xml version="1.0" encoding="UTF-8"?>
<!-- 
  Copyright (C) 2018 Jason Reed
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
--> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" standalone="no" omit-xml-declaration="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:attribute-set name="myBorder">
  		<xsl:attribute name="border">solid 0.4mm black</xsl:attribute>
  		<xsl:attribute name="height">50mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="rightJustify">
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="padding-right">8mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="cardSpacing">
		<xsl:attribute name="height">10mm</xsl:attribute>
		<xsl:attribute name="padding">2mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="dashedBorder">
		<xsl:attribute name="border">dashed 0.4mm black</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="smallFont">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:variable name="customerNumber" select="/list/header/customerNumber" />
	<xsl:param name="pColumns" select="4" />
	<xsl:template match="/list">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="basar-labels" page-width="210mm" page-height="297mm">
					<fo:region-body margin="5mm" />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="basar-labels">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates />
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	<xsl:template match="header" />
	<xsl:template match="body/items">
		<fo:table>
			<fo:table-column column-width="50mm" />
			<fo:table-column column-width="50mm" />
			<fo:table-column column-width="50mm" />
			<fo:table-column column-width="50mm" />
			<fo:table-body>
				<xsl:for-each-group select="item" group-by="(position() -1) idiv $pColumns">
					<fo:table-row>
						<xsl:for-each-group select="current-group()" group-by="(position() -1) idiv $pColumns">
							<xsl:apply-templates select="current-group()" mode="cell" />
						</xsl:for-each-group>
					</fo:table-row>
				</xsl:for-each-group>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<xsl:template match="item" mode="cell">
		<fo:table-cell xsl:use-attribute-sets="myBorder">
			<fo:table>
				<fo:table-column column-width="50mm" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="cardSpacing">
							<fo:block  xsl:use-attribute-sets="rightJustify">
								<xsl:value-of select="$customerNumber" />-<xsl:value-of select="number" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="smallFont cardSpacing">
							<fo:block>
								<xsl:value-of select="description" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="cardSpacing">
							<fo:block>
								<xsl:choose>
									<xsl:when test="size != ''">
										Größe: <xsl:value-of select="size" />	
									</xsl:when>
									<xsl:otherwise>
										&#160;
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="cardSpacing">
							<fo:block>
								 Preis: €<xsl:value-of select="price" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="dashedBorder cardSpacing">
							<fo:table>
								<fo:table-column column-width="50mm" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="$customerNumber" />-<xsl:value-of select="number" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell xsl:use-attribute-sets="rightJustify">
											<fo:block>
												 €<xsl:value-of select="price" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:table-cell>
	</xsl:template>
</xsl:stylesheet>
