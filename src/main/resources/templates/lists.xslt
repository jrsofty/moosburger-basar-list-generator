<?xml version="1.0" encoding="UTF-8"?>
<!-- 
  Copyright (C) 2018 Jason Reed
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
--> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" standalone="no" omit-xml-declaration="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:attribute-set name="myBorder">
  		<xsl:attribute name="border">solid 0.1mm black</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="large-font">
		<xsl:attribute name="font-size">13pt</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="bold-font">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="normal-font">
		<xsl:attribute name="font-size">11pt</xsl:attribute>
		<xsl:attribute name="padding-left">1mm</xsl:attribute>
	</xsl:attribute-set>
	<xsl:attribute-set name="list-cell">
		<xsl:attribute name="padding-left">2mm</xsl:attribute>
		<xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="padding-bottom">2mm</xsl:attribute>
		<xsl:attribute name="padding-right">2mm</xsl:attribute>
	</xsl:attribute-set>
	
	<xsl:template match="/list">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="basar-list" page-width="210mm" page-height="297mm">
					<fo:region-body margin="5mm" />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="basar-list">
				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates />
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template match="header">
		<fo:table>
			<fo:table-column column-width="33.3mm" />
			<fo:table-column column-width="33.3mm" />
			<fo:table-column column-width="33.3mm" />
			<fo:table-column column-width="33.3mm" />
			<fo:table-column column-width="33.3mm" />
			<fo:table-column column-width="33.3mm" />
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="large-font bold-font">
							Basarnummer:
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="large-font">
							<xsl:choose>
								<xsl:when test="customerNumber != ''">
									<xsl:value-of select="customerNumber" />	
								</xsl:when>
								<xsl:otherwise>
									&#160;
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell >
						<fo:block xsl:use-attribute-sets="large-font bold-font">
							Name, Vorname:
						</fo:block>
					</fo:table-cell>
					<fo:table-cell >
						<fo:block xsl:use-attribute-sets="large-font">
							<xsl:choose>
								<xsl:when test="lastName != '' and firstName != ''">
									<xsl:value-of select="lastName" />,&#160;<xsl:value-of select="firstName" />
								</xsl:when>
								<xsl:otherwise>
									&#160;
								</xsl:otherwise>
							</xsl:choose>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell >
						<fo:block xsl:use-attribute-sets="large-font bold-font">
							Behälter:
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block xsl:use-attribute-sets="large-font">
							<xsl:choose>
								<xsl:when test="container != ''">
									<xsl:value-of select="container" />	
								</xsl:when>
								<xsl:otherwise>
									&#160;
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<xsl:template match="body">
		<fo:table>
			<fo:table-column column-width="15mm" />
			<fo:table-column column-width="155mm" />
			<fo:table-column column-width="15mm" />
			<fo:table-column column-width="15mm" />
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
						<fo:block xsl:use-attribute-sets="normal-font bold-font">
							lfde.Nr.
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
						<fo:block xsl:use-attribute-sets="normal-font bold-font">
							Beschreibung
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
						<fo:block xsl:use-attribute-sets="normal-font bold-font">
							Größe
						</fo:block>
					</fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
						<fo:block xsl:use-attribute-sets="normal-font bold-font">
							Preis (€)
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>			
			<fo:table-body>
				<xsl:for-each select="items/item">
					<fo:table-row>
						<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
							<fo:block xsl:use-attribute-sets="normal-font">
								<xsl:value-of select="number" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
							<fo:block xsl:use-attribute-sets="normal-font">
								<xsl:choose>
									<xsl:when test="description != ''">
										<xsl:value-of select="description" />	
									</xsl:when>
									<xsl:otherwise>
										&#160;
									</xsl:otherwise>
								</xsl:choose>								
							</fo:block>
						</fo:table-cell>
						<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
							<fo:block xsl:use-attribute-sets="normal-font">
								<xsl:choose>
									<xsl:when test="size != ''">
										<xsl:value-of select="size" />	
									</xsl:when>
									<xsl:otherwise>
										&#160;
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell xsl:use-attribute-sets="list-cell myBorder">
							<fo:block xsl:use-attribute-sets="normal-font">
								<xsl:choose>
									<xsl:when test="price != ''">
										€<xsl:value-of select="price" />
									</xsl:when>
									<xsl:otherwise>
										&#160;
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
