/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.data.controller;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import org.bitbucket.jrsofty.mblg.ApplicationMain;
import org.bitbucket.jrsofty.mblg.data.ListDocument;
import org.bitbucket.jrsofty.mblg.exceptions.InvalidListDocumentException;
import org.bitbucket.jrsofty.mblg.exceptions.ListDocumentAlreadyExistsException;
import org.bitbucket.jrsofty.mblg.exceptions.ListDocumentNotFoundException;

/**
 *
 * @author Jason Reed
 *
 */
public interface ListDocumentFactory {

    ListDocumentFactory impl = new ListDocumentFactoryImpl();

    static ListDocumentFactory getFactory() {
        return ListDocumentFactory.impl;
    }

    ListDocument newListDocument();

    ListDocument newListDocument(UUID id) throws ListDocumentAlreadyExistsException;

    ListDocument getListDocument(UUID id) throws ListDocumentNotFoundException;

    void cacheListDocument(ListDocument document) throws InvalidListDocumentException, ListDocumentAlreadyExistsException;

    void removeListDocument(UUID id) throws ListDocumentNotFoundException;

    Set<UUID> getIds();

    Collection<ListDocument> getListDocuments();

    class ListDocumentFactoryImpl implements ListDocumentFactory {
        private final HashMap<UUID, ListDocument> cache = new HashMap<>();

        @Override
        public ListDocument newListDocument() {
            final UUID id = UUID.randomUUID();
            final ListDocument document = new ListDocument(id);
            document.getMeta().setCreationDate(new Date());
            document.getMeta().setListName("Neue Liste");
            this.setDefaultValues(document);

            this.cache.put(id, document);
            return document;
        }

        @Override
        public ListDocument newListDocument(final UUID id) throws ListDocumentAlreadyExistsException {
            if (this.cache.containsKey(id)) {
                throw new ListDocumentAlreadyExistsException(id);
            }
            final ListDocument document = new ListDocument(id);
            document.getMeta().setCreationDate(new Date());
            document.getMeta().setListName("Neue Liste");
            this.setDefaultValues(document);
            this.cache.put(id, document);
            return document;
        }

        @Override
        public ListDocument getListDocument(final UUID id) throws ListDocumentNotFoundException {
            if (!this.cache.containsKey(id)) {
                throw new ListDocumentNotFoundException(id);
            }

            return this.cache.get(id);
        }

        @Override
        public void cacheListDocument(final ListDocument document) throws InvalidListDocumentException, ListDocumentAlreadyExistsException {
            final UUID id = document.getId();
            if (null == id) {
                throw new InvalidListDocumentException(document);
            }
            if (this.cache.containsKey(id)) {
                throw new ListDocumentAlreadyExistsException(id);
            }

            this.cache.put(id, document);

        }

        @Override
        public void removeListDocument(final UUID id) throws ListDocumentNotFoundException {
            if (!this.cache.containsKey(id)) {
                throw new ListDocumentNotFoundException(id);
            }

            this.cache.remove(id);
        }

        @Override
        public Set<UUID> getIds() {
            return this.cache.keySet();
        }

        @Override
        public Collection<ListDocument> getListDocuments() {

            return this.cache.values();
        }

        private void setDefaultValues(final ListDocument document) {
            final String firstName = ApplicationMain.CONF.getDefaultFirstName();
            final String lastName = ApplicationMain.CONF.getDefaultLastName();
            final String basarNumber = ApplicationMain.CONF.getDefaultBasarNumber();

            document.getHeader().setFirstName(firstName);
            document.getHeader().setLastName(lastName);
            document.getHeader().setCustomerNumber(basarNumber);
        }

    }

}
