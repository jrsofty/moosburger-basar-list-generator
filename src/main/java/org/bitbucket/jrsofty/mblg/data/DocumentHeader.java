/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.data;

import java.io.Serializable;

/**
 *
 * @author Jason Reed
 *
 */
public class DocumentHeader implements Serializable, Cloneable {
    /**
     *
     */
    private static final long serialVersionUID = -8879098913017450943L;
    private ListType listType = ListType.KINDERKLEIDER;
    private String lastName = "";
    private String firstName = "";
    private String customerNumber = "";
    private String container = "";

    public ListType getListType() {
        return this.listType;
    }

    public void setListType(final ListType listType) {
        this.listType = listType;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getCustomerNumber() {
        return this.customerNumber;
    }

    public void setCustomerNumber(final String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getContainer() {
        return this.container;
    }

    public void setContainer(final String container) {
        this.container = container;
    }

    @Override
    public Object clone() {
        final DocumentHeader header = new DocumentHeader();
        header.setContainer(this.container);
        header.setCustomerNumber(this.customerNumber);
        header.setFirstName(this.firstName);
        header.setLastName(this.lastName);
        header.setListType(this.listType);
        return header;
    }
}
