/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.data;

import java.io.Serializable;
import java.util.UUID;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 *
 * @author Jason Reed
 *
 */
@XStreamAlias("list")
public class ListDocument implements Serializable, Cloneable {
    /**
     *
     */
    private static final long serialVersionUID = 6082682468244157665L;

    private final UUID id;
    @XStreamOmitField
    private boolean dirty = false;
    private DocumentMeta meta = new DocumentMeta();
    private DocumentHeader header = new DocumentHeader();
    private DocumentBody body = new DocumentBody();

    public ListDocument(final UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return this.id;
    }

    public DocumentHeader getHeader() {
        return this.header;
    }

    public void setHeader(final DocumentHeader header) {
        this.header = header;
    }

    public DocumentBody getBody() {
        return this.body;
    }

    public void setBody(final DocumentBody body) {
        this.body = body;
    }

    public void setMeta(final DocumentMeta meta) {
        this.meta = meta;
    }

    public DocumentMeta getMeta() {
        return this.meta;
    }

    public boolean isDirty() {
        return this.dirty;
    }

    public void setDirty(final boolean dirty) {
        this.dirty = dirty;
    }

    @Override
    public Object clone() {
        final ListDocument document = new ListDocument(this.id);
        document.setMeta((DocumentMeta) this.meta.clone());
        document.setBody((DocumentBody) this.body.clone());
        document.setHeader((DocumentHeader) this.header.clone());
        document.setDirty(this.dirty);
        return document;
    }

}
