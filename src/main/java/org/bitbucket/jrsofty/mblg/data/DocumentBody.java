/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bitbucket.jrsofty.mblg.exceptions.ItemAlreadyExistsException;
import org.bitbucket.jrsofty.mblg.exceptions.ItemNotFoundException;

/**
 *
 * @author Jason Reed
 *
 */
public class DocumentBody implements Serializable, Cloneable {
    /**
     *
     */
    private static final long serialVersionUID = -6074818194450342683L;

    private ArrayList<ListItem> items = new ArrayList<>();

    public ArrayList<ListItem> getItems() {
        return this.items;
    }

    public void setItems(final ArrayList<ListItem> items) {
        this.items = items;
    }

    public void addItem(final ListItem item) throws ItemAlreadyExistsException {
        if (this.doesItemExist(item.getInternalId())) {
            throw new ItemAlreadyExistsException(item);
        }

        this.items.add(item);
        this.updateNumbers();

    }

    public void addAllItems(final List<ListItem> items) {
        this.items.addAll(items);
        this.updateNumbers();
    }

    public ListItem getItem(final UUID internalId) throws ItemNotFoundException {
        ListItem result = null;
        for (final ListItem item : this.items) {
            if (item.getInternalId().equals(internalId)) {
                result = item;
                break;
            }
        }
        if (null == result) {
            throw new ItemNotFoundException(internalId);
        }

        return result;
    }

    public void delItem(final ListItem item) {
        this.items.remove(item);
        this.updateNumbers();
    }

    private boolean doesItemExist(final UUID internalId) {
        boolean result = false;
        for (final ListItem item : this.items) {
            if (item.getInternalId().equals(internalId)) {
                result = true;
                break;
            }
        }
        return result;
    }

    public void updateNumbers() {
        int number = 1;
        for (final ListItem item : this.items) {
            item.setNumber(number);
            number++;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object clone() {
        final DocumentBody body = new DocumentBody();
        body.setItems((ArrayList<ListItem>) this.getItems().clone());
        return body;
    }

}
