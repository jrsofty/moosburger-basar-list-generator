package org.bitbucket.jrsofty.mblg.data;

import java.io.Serializable;
import java.util.Date;

public class DocumentMeta implements Serializable, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = 1200677430255782609L;
    private Date creationDate;
    private Date modifiedDate;
    private String listName;
    private String listFilePath;

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getListName() {
        return this.listName;
    }

    public void setListName(final String listName) {
        this.listName = listName;
    }

    public String getListFilePath() {
        return this.listFilePath;
    }

    public void setListFilePath(final String listFilePath) {
        this.listFilePath = listFilePath;
    }

    public Date getModifiedDate() {
        return this.modifiedDate;
    }

    public void setModifiedDate(final Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public Object clone() {
        final DocumentMeta meta = new DocumentMeta();
        meta.setCreationDate(this.creationDate);
        meta.setListFilePath(this.listFilePath);
        meta.setListName(this.listName);
        meta.setModifiedDate(this.modifiedDate);
        return meta;
    }

}
