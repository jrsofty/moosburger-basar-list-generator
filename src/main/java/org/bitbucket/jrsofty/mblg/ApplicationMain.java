/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;

import javax.print.PrintService;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.bitbucket.jrsofty.mblg.engine.FileIO;
import org.bitbucket.jrsofty.mblg.util.AppConf;
import org.bitbucket.jrsofty.mblg.util.ResourceLoader;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.panda_lang.pandomium.Pandomium;
import org.panda_lang.pandomium.settings.PandomiumSettings;
import org.panda_lang.pandomium.wrapper.PandomiumBrowser;
import org.panda_lang.pandomium.wrapper.PandomiumClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Jason Reed
 *
 */

public class ApplicationMain {

    public static ApplicationMain IMPL;
    public static AppConf CONF;
    private static Logger LOG = LoggerFactory.getLogger(ApplicationMain.class);

    private JFrame frame;
    private Server server;
    private PandomiumBrowser browser;

    public static void main(final String[] args) throws Exception {
        ApplicationMain.LOG.info("Application Starting ...");

        ApplicationMain.CONF = new AppConf();
        ApplicationMain.LOG.info("... Configuration loaded ...");

        ApplicationMain.IMPL = new ApplicationMain();
        ApplicationMain.IMPL.initializeApplication();

        ApplicationMain.LOG.info("... Application Ends");

    }

    public String displayOpenFileDialog(final String startingPath) {

        final JFileChooser openFileDialog = new JFileChooser(startingPath);
        final FileNameExtensionFilter filter = new FileNameExtensionFilter("MBLG Files", "mblg");
        openFileDialog.setFileFilter(filter);
        final int returnval = openFileDialog.showOpenDialog(this.frame);
        if (returnval == JFileChooser.APPROVE_OPTION) {
            return openFileDialog.getSelectedFile().getAbsolutePath();
        }
        return "";

    }

    public String displaySaveFileDialog(final String startingPath) {
        final JFileChooser saveFileDialog = new JFileChooser(startingPath);
        final FileNameExtensionFilter filter = new FileNameExtensionFilter("MBLG Files", "mblg");
        saveFileDialog.setFileFilter(filter);
        final int returnval = saveFileDialog.showSaveDialog(this.frame);
        if (returnval == JFileChooser.APPROVE_OPTION) {
            return saveFileDialog.getSelectedFile().getAbsolutePath();
        }
        return "";
    }

    public boolean printWithDialog(final byte[] dataToPrint) {
        try {
            this.printDocument(dataToPrint);
            return true;
        } catch (IOException | PrinterException e) {
            ApplicationMain.LOG.error("Failed to open printer dialog", e);
            return false;
        }

    }

    private void printDocument(final byte[] document) throws InvalidPasswordException, IOException, PrinterException {
        final PrinterJob job = PrinterJob.getPrinterJob();
        PrintService printer = null;
        if (job.printDialog()) {
            printer = job.getPrintService();
        } else {
            return;
        }

        FileIO.printPDF(document, job, printer);

    }

    public void initializeApplication() throws Exception {
        if (ApplicationMain.LOG.isDebugEnabled()) {
            ApplicationMain.LOG.debug("Begin application initialization");
        }
        this.initializeServer();
        if (ApplicationMain.LOG.isDebugEnabled()) {
            ApplicationMain.LOG.debug("Web server is initialized");
        }
        this.initializeBrowser();
        if (ApplicationMain.LOG.isDebugEnabled()) {
            ApplicationMain.LOG.debug("Browser Initialized");
        }
        this.initializeGUI();
        if (ApplicationMain.LOG.isDebugEnabled()) {
            ApplicationMain.LOG.debug("GUI initialized");
        }

        this.server.join();
    }

    private void initializeGUI() {
        this.frame = new JFrame();
        if (ApplicationMain.CONF.getDebugMode()) {
            this.initDebugMenu();
        }
        this.frame.getContentPane().add(this.browser.toAWTComponent(), BorderLayout.CENTER);

        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                ApplicationMain.this.frame.dispose();
            }
        });
        try {
            this.frame.setIconImage(ResourceLoader.getIconImage(ResourceLoader.ICON_PRINT_LIST));
        } catch (final IOException e) {
            e.printStackTrace();
        }
        this.frame.setTitle("Moosburger Basar List");
        this.frame.setSize(1720, 840);
        this.frame.setVisible(true);

    }

    private void initDebugMenu() {
        if (ApplicationMain.LOG.isDebugEnabled()) {
            ApplicationMain.LOG.debug("### DEBUG MODE IDENTIFIED ###");
        }
        final JMenuBar debugMenuBar = new JMenuBar();
        final JMenu debugMenu = new JMenu();
        debugMenu.setText("Debug");
        final JMenuItem refresh = new JMenuItem();
        refresh.setText("Refresh");
        refresh.addActionListener(e -> this.refreshBrowser());
        debugMenu.add(refresh);
        debugMenuBar.add(debugMenu);
        this.frame.setJMenuBar(debugMenuBar);

    }

    private void initializeBrowser() throws Exception {
        final Pandomium pandomium = new Pandomium(PandomiumSettings.getDefaultSettings());

        pandomium.initialize();

        final PandomiumClient client = pandomium.createClient();
        final String initialUrl = "http://localhost:" + ApplicationMain.CONF.getServicePort();

        this.browser = client.loadURL(initialUrl);
    }

    private void initializeServer() throws Exception {

        this.server = new Server(ApplicationMain.CONF.getServicePort());

        final ResourceHandler context = new ResourceHandler();
        final ServletContextHandler srvletContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
        srvletContext.setContextPath("/");

        final ServletHolder jerseyServlet = srvletContext.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/rest/*");
        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter("jersey.config.server.provider.packages", "org.bitbucket.jrsofty.mblg.rest");

        final String webdir = ResourceLoader.getWebDirectoryAsExternal("site");
        context.setDirectoriesListed(true);
        context.setResourceBase(webdir);
        context.setWelcomeFiles(this.buildWelcomeFilesArray());

        final HandlerList handlers = new HandlerList(context, srvletContext);
        this.server.setHandler(handlers);

        this.server.start();
    }

    private String[] buildWelcomeFilesArray() {
        final String lang = ApplicationMain.CONF.getConfiguredLanguage();
        final String[] welcomeFiles = new String[1];
        welcomeFiles[0] = "index.html";
        if (lang.equalsIgnoreCase("de")) {
            welcomeFiles[0] = "index_de.html";
        }
        return welcomeFiles;
    }

    private void refreshBrowser() {
        this.browser.getCefBrowser().reloadIgnoreCache();
    }

}
