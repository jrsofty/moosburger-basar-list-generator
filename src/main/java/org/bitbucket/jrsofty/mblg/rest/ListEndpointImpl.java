package org.bitbucket.jrsofty.mblg.rest;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.bitbucket.jrsofty.mblg.data.DocumentMeta;
import org.bitbucket.jrsofty.mblg.data.ListDocument;
import org.bitbucket.jrsofty.mblg.data.ListItem;
import org.bitbucket.jrsofty.mblg.data.ListType;
import org.bitbucket.jrsofty.mblg.data.controller.ListDocumentFactory;
import org.bitbucket.jrsofty.mblg.engine.FileIO;
import org.bitbucket.jrsofty.mblg.exceptions.InvalidListDocumentException;
import org.bitbucket.jrsofty.mblg.exceptions.InvalidXmlException;
import org.bitbucket.jrsofty.mblg.exceptions.ItemAlreadyExistsException;
import org.bitbucket.jrsofty.mblg.exceptions.ItemNotFoundException;
import org.bitbucket.jrsofty.mblg.exceptions.ListDocumentAlreadyExistsException;
import org.bitbucket.jrsofty.mblg.exceptions.ListDocumentNotFoundException;
import org.bitbucket.jrsofty.mblg.util.DocumentConverter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/list")
public class ListEndpointImpl {

    private static Logger LOG = LoggerFactory.getLogger(ListEndpointImpl.class);

    @GET
    @Path("/all")
    @Produces("application/json; charset=utf-8")
    public Response getAllListIds() {
        final HashSet<ListDocument> documents = new HashSet<>();
        documents.addAll(ListDocumentFactory.impl.getListDocuments());
        final JSONArray result = new JSONArray(documents);
        return Response.ok(result.toString(4)).build();
    }

    @GET
    @Path("/new")
    @Produces("application/json; charset=utf-8")
    public Response createNewList() {
        final UUID id = UUID.randomUUID();
        JSONObject responseObject;
        try {
            final ListDocument document = ListDocumentFactory.impl.newListDocument(id);
            responseObject = new JSONObject(document);
            return Response.ok(responseObject.toString(4)).build();
        } catch (final ListDocumentAlreadyExistsException e) {
            responseObject = new JSONObject();
            responseObject.put("status", 500);
            responseObject.put("msg", "Could not create a new list.");
            return Response.serverError().entity(responseObject.toString(4)).build();
        }
    }

    @GET
    @Path("/closeList/{id}")
    @Produces("application/json; charset=utf-8")
    public Response closeList(@PathParam("id") final String id) {
        final UUID actualId = UUID.fromString(id);
        final JSONObject responseObject = new JSONObject();
        try {
            final ListDocument document = ListDocumentFactory.impl.getListDocument(actualId);

            if (document.isDirty()) {
                responseObject.put("status", 300);
                responseObject.put("id", id);
                responseObject.put("msg", "Document not saved");
            } else {
                ListDocumentFactory.getFactory().removeListDocument(actualId);
                responseObject.put("status", 200);
                responseObject.put("id", id);
                responseObject.put("msg", "Document closed");
            }

            return Response.ok(responseObject.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            responseObject.put("status", 404);
            responseObject.put("msg", "Could not find document.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        }

    }

    @POST
    @Path("/{id}/add")
    @Produces("application/json; charset=utf-8")
    @Consumes("application/json; charset=utf-8")
    public Response addItemToList(@PathParam("id") final String id, final String itemContent) {
        final UUID listId = UUID.fromString(id);
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(listId);
            final JSONObject itemJson = new JSONObject(itemContent);
            ListItem item = new ListItem();
            final UUID itemId = UUID.randomUUID();
            item.setInternalId(itemId);
            item.setDescription(itemJson.getString("description"));
            item.setPrice(this.formatPrice(itemJson.getString("price")));
            if (itemJson.has("size")) {
                item.setSize(itemJson.getString("size"));
            }
            document.getBody().addItem(item);
            document.setDirty(true);
            item = document.getBody().getItem(itemId);
            final JSONObject result = new JSONObject(item);
            return Response.ok(result.toString(4)).build();
        } catch (final ListDocumentNotFoundException | ItemAlreadyExistsException | ItemNotFoundException e) {
            final JSONObject errorResponse = this.createErrorResponseObject(500, "Failed to add item to list");
            return Response.serverError().status(500).entity(errorResponse.toString(4)).build();
        }
    }

    @POST
    @Path("/{id}/modify/{item}")
    @Produces("application/json; charset=utf-8")
    @Consumes("application/json; charset=utf-8")
    public Response updateItemInList(@PathParam("id") final String listId, @PathParam("item") final String itemId, final String itemContent) {
        final UUID actualListId = UUID.fromString(listId);
        final UUID actualItemId = UUID.fromString(itemId);
        final JSONObject itemJson = new JSONObject(itemContent);
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            final ListItem item = document.getBody().getItem(actualItemId);
            item.setDescription(itemJson.getString("description"));
            item.setPrice(this.formatPrice(itemJson.getString("price")));
            item.setSize(itemJson.getString("size"));
            document.setDirty(true);
            final JSONObject result = new JSONObject(item);
            return Response.ok(result.toString(4)).build();
        } catch (ListDocumentNotFoundException | ItemNotFoundException e) {
            return Response.status(404).build();
        }

    }

    @GET
    @Path("/get/{id}")
    @Produces("application/json; charset=utf-8")
    public Response getList(@PathParam("id") final String id) {
        final UUID listId = UUID.fromString(id);
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(listId);
            final JSONObject result = new JSONObject(document);
            return Response.ok(result.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            final JSONObject errorResponse = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(errorResponse.toString(4)).build();
        }

    }

    @GET
    @Path("/{id}/remove/{itemId}")
    @Produces("application/json; charset=utf-8")
    public Response removeItemFromList(@PathParam("id") final String listId, @PathParam("itemId") final String itemId) {
        final UUID actualListId = UUID.fromString(listId);
        final UUID actualItemId = UUID.fromString(itemId);
        JSONObject responseObject;
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            final ListItem item = document.getBody().getItem(actualItemId);
            document.setDirty(true);
            document.getBody().delItem(item);
            responseObject = new JSONObject(document);
            return Response.ok(responseObject.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {

            responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().entity(responseObject.toString(4)).build();
        } catch (final ItemNotFoundException e) {
            responseObject = this.createErrorResponseObject(404, "Item not found in document");
            return Response.serverError().entity(responseObject.toString(4)).build();
        }
    }

    @GET
    @Path("/{id}/count")
    @Produces("application/json; charset=utf-8")
    public Response countNumberOfItems(@PathParam("id") final String listId) {
        final UUID acutualListId = UUID.fromString(listId);
        JSONObject responseObject;
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(acutualListId);
            responseObject = new JSONObject();
            responseObject.put("custNo", document.getHeader().getCustomerNumber());
            responseObject.put("listType", document.getHeader().getListType());
            responseObject.put("items", document.getBody().getItems().size());
            return Response.ok().entity(responseObject.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            responseObject = this.createErrorResponseObject(404, "List document not found");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        }

    }

    @GET
    @Path("/get/{id}/item/{itemId}")
    @Produces("application/json; charset=utf-8")
    public Response getItemFromList(@PathParam("id") final String listId, @PathParam("itemId") final String itemId) {
        final UUID actualListId = UUID.fromString(listId);
        final UUID actualItemId = UUID.fromString(itemId);
        JSONObject responseObject;
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            final ListItem item = document.getBody().getItem(actualItemId);
            responseObject = new JSONObject(item);
            return Response.ok(responseObject.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        } catch (final ItemNotFoundException e) {
            responseObject = this.createErrorResponseObject(404, "Item not found in list.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        }
    }

    @GET
    @Path("/{id}/setType/{listType}")
    @Produces("application/json; charset=utf-8")
    public Response setListType(@PathParam("id") final String listId, @PathParam("listType") final String listType) {
        final UUID actualListId = UUID.fromString(listId);
        final ListType actualListType = ListType.getByStringValue(listType.toUpperCase());

        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            document.setDirty(true);
            document.getHeader().setListType(actualListType);
            final JSONObject object = new JSONObject(document);
            return Response.ok(object.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            final JSONObject responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        }

    }

    @GET
    @Path("/{id}/setContainer/{container}")
    @Produces("application/json; charset=utf-8")
    public Response setContainer(@PathParam("id") final String listId, @PathParam("container") final String container) {
        final UUID actualListId = UUID.fromString(listId);

        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            document.setDirty(true);
            document.getHeader().setContainer(container);
            final JSONObject object = new JSONObject(document);
            return Response.ok(object.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            final JSONObject responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        }

    }

    @GET
    @Path("/{id}/setFirstName/{firstName}")
    @Produces("application/json; charset=utf-8")
    public Response setFirstName(@PathParam("id") final String listId, @PathParam("firstName") final String firstName) {
        final UUID actualListId = UUID.fromString(listId);

        try {
            final String actualName = URLDecoder.decode(firstName, "UTF-8");
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            document.setDirty(true);
            document.getHeader().setFirstName(actualName);
            final JSONObject object = new JSONObject(document);
            return Response.ok(object.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            final JSONObject responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        } catch (final UnsupportedEncodingException e) {
            final JSONObject responseObject = this.createErrorResponseObject(500, "Unexpected encoding error.");
            return Response.serverError().status(500).entity(responseObject.toString(4)).build();
        }
    }

    @GET
    @Path("/{id}/setLastName/{lastName}")
    @Produces("application/json; charset=utf-8")
    public Response setLastName(@PathParam("id") final String listId, @PathParam("lastName") final String lastName) {
        final UUID actualListId = UUID.fromString(listId);

        try {
            final String actualName = URLDecoder.decode(lastName, "UTF-8");
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            document.setDirty(true);
            document.getHeader().setLastName(actualName);
            final JSONObject object = new JSONObject(document);
            return Response.ok(object.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            final JSONObject responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        } catch (final UnsupportedEncodingException e) {
            final JSONObject responseObject = this.createErrorResponseObject(500, "Unexpected encoding error.");
            return Response.serverError().status(500).entity(responseObject.toString(4)).build();
        }
    }

    @GET
    @Path("/{id}/setCustomerNumber/{custno}")
    @Produces("application/json; charset=utf-8")
    public Response setCustomerNumber(@PathParam("id") final String listId, @PathParam("custno") final String customerNumber) {
        final UUID actualListId = UUID.fromString(listId);

        try {
            final String actualName = URLDecoder.decode(customerNumber, "UTF-8");
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            document.setDirty(true);
            document.getHeader().setCustomerNumber(actualName);
            final JSONObject object = new JSONObject(document);
            return Response.ok(object.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            final JSONObject responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        } catch (final UnsupportedEncodingException e) {
            final JSONObject responseObject = this.createErrorResponseObject(500, "Unexpected encoding error.");
            return Response.serverError().status(500).entity(responseObject.toString(4)).build();
        }
    }

    @POST
    @Path("/load")
    @Produces("application/json; charset=utf-8")
    @Consumes("application/json; charset=utf-8")
    public Response loadFile(final String fileRequestInformation) {
        final JSONObject requestObject = new JSONObject(fileRequestInformation);
        JSONObject responseObject = new JSONObject();
        String requestedFilePath;
        if (requestObject.has("filepath")) {
            requestedFilePath = requestObject.getString("filepath");
        } else {
            responseObject = this.createErrorResponseObject(400, "Missing filepath property");
            ListEndpointImpl.LOG.error("Malformed request");
            return Response.serverError().status(400).entity(responseObject).build();
        }

        final File fileToLoad = new File(requestedFilePath);
        if (!fileToLoad.exists()) {
            responseObject.put("status", 404);
            responseObject.put("msg", "File not found");
            ListEndpointImpl.LOG.error("File not found");
            return Response.serverError().status(404).entity(responseObject).build();
        }

        try {
            final String fileData = FileIO.getDataFromFile(fileToLoad);
            final DocumentConverter convert = new DocumentConverter();
            final ListDocument document = convert.convertXmlStringToDocument(fileData);
            if (null == document.getMeta()) {
                document.setMeta(new DocumentMeta());
            }

            if ((null == document.getMeta().getListName()) || document.getMeta().getListName().isEmpty()) {
                final String listName = fileToLoad.getName();
                document.getMeta().setListName(listName);
            }

            document.getMeta().setCreationDate(new Date());
            document.getMeta().setListFilePath(requestedFilePath);

            ListDocumentFactory.getFactory().cacheListDocument(document);
            responseObject = new JSONObject(document);
            return Response.ok(responseObject.toString(4)).build();
        } catch (final IOException | InvalidXmlException | InvalidListDocumentException | ListDocumentAlreadyExistsException e) {
            responseObject.put("status", 500);
            responseObject.put("msg", "Could not load file data.");
            ListEndpointImpl.LOG.error("Failure in final loading.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseObject).build();
        }

    }

    @GET
    @Path("{id}/isDirty")
    @Produces("application/json; charset=utf-8")
    public Response getDirtyStatus(@PathParam("id") final String listId) {
        final UUID actualListId = UUID.fromString(listId);
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(actualListId);
            final JSONObject result = new JSONObject();
            result.put("dirty", document.isDirty());
            return Response.ok(result.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            final JSONObject responseObject = this.createErrorResponseObject(404, "List document not found.");
            return Response.serverError().status(404).entity(responseObject.toString(4)).build();
        }
    }

    @POST
    @Path("/saveAs/{id}")
    @Produces("application/json; charset=utf-8")
    @Consumes("application/json; charset=utf-8")
    public Response saveFileAs(@PathParam("id") final String listId, final String data) {
        final UUID id = UUID.fromString(listId);
        final JSONObject request = new JSONObject(data);
        JSONObject response = new JSONObject();
        final DocumentConverter converter = new DocumentConverter();
        if (!request.has("filepath") || request.getString("filepath").isEmpty()) {
            return Response.status(400).build();
        }

        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(id);
            final File saveFile = new File(request.getString("filepath"));
            document.getMeta().setListName(saveFile.getName());
            final String xml = converter.convertDocumentToXmlString(document);
            FileIO.saveDataToFile(xml.getBytes("UTF-8"), saveFile);
            document.getMeta().setListFilePath(request.getString("filepath"));
            document.setDirty(false);
            response = new JSONObject(document);
            return Response.ok(response.toString(4)).build();
        } catch (final ListDocumentNotFoundException e) {
            return Response.status(404).build();
        } catch (final UnsupportedEncodingException e) {
            return Response.status(500).build();
        } catch (final IOException e) {
            return Response.status(500).build();
        }
    }

    @GET
    @Path("/save/{id}")
    @Produces("application/json; charset=utf-8")
    public Response saveFile(@PathParam("id") final String id) {
        final UUID listId = UUID.fromString(id);
        JSONObject response = new JSONObject();
        final DocumentConverter converter = new DocumentConverter();
        try {
            final ListDocument document = ListDocumentFactory.getFactory().getListDocument(listId);
            final String filepath = document.getMeta().getListFilePath();
            // removed the filepath.isBlank() as this is only for Java 11
            if ((null == filepath) || filepath.isEmpty()) {
                response.put("status", "700");
                return Response.ok(response.toString(4)).build();
            }
            final File file = new File(filepath);
            document.getMeta().setListName(file.getName());

            final String xml = converter.convertDocumentToXmlString(document);
            FileIO.saveDataToFile(xml.getBytes("UTF-8"), file);
            document.getMeta().setListFilePath(filepath);
            document.setDirty(false);
            response = new JSONObject(document);
            return Response.ok(response.toString(4)).build();

        } catch (final ListDocumentNotFoundException e) {
            ListEndpointImpl.LOG.error("Could not find document", e);
            return Response.status(404).build();
        } catch (final UnsupportedEncodingException e) {
            ListEndpointImpl.LOG.error("Failed encoding file data", e);
            return Response.serverError().build();
        } catch (final IOException e) {
            ListEndpointImpl.LOG.error("Failed to save file", e);
            return Response.serverError().build();
        }

    }

    private String formatPrice(String price) {
        final int comma = price.indexOf(',');
        final int paddingCheck = price.length() - 3;
        final int trimmingCheck = comma + 3;
        if (comma == 0) {
            price = "0" + price;
        }

        if (paddingCheck < comma) {
            final int fix = comma - paddingCheck;
            for (int i = 0; i < fix; i++) {
                price = price + "0";
            }
        }

        if (trimmingCheck < price.length()) {
            price = price.substring(0, trimmingCheck);
        }

        return price;
    }

    private JSONObject createErrorResponseObject(final int status, final String msg) {
        final JSONObject responseObject = new JSONObject();
        responseObject.put("status", status);
        responseObject.put("msg", msg);
        return responseObject;
    }

}
