package org.bitbucket.jrsofty.mblg.rest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import org.bitbucket.jrsofty.mblg.ApplicationMain;
import org.bitbucket.jrsofty.mblg.data.ListDocument;
import org.bitbucket.jrsofty.mblg.data.ListItem;
import org.bitbucket.jrsofty.mblg.data.controller.ListDocumentFactory;
import org.bitbucket.jrsofty.mblg.engine.DocumentGenerator;
import org.bitbucket.jrsofty.mblg.exceptions.ItemAlreadyExistsException;
import org.bitbucket.jrsofty.mblg.exceptions.ListDocumentNotFoundException;
import org.bitbucket.jrsofty.mblg.util.DocumentConverter;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

@Path("/control")
public class ProgramControlEndpointImpl {

    @GET
    @Path("/openFileDialog")
    @Produces("application/json; charset=utf-8")
    public Response openDialog() {
        final String startingPath = ApplicationMain.CONF.getLastUsedDirectoryString();
        final String filepath = ApplicationMain.IMPL.displayOpenFileDialog(startingPath);
        final JSONObject result = new JSONObject();
        try {
            result.put("filepath", filepath);
        } catch (final JSONException e) {
            return this.generateErrorResponseObject(500, "Failed to build response correctly " + e.getMessage());
        }
        return Response.ok().entity(result.toString(4)).build();
    }

    @GET
    @Path("/saveFileDialog")
    @Produces("application/json; charset=utf-8")
    public Response saveDialog() {
        final String startingPath = ApplicationMain.CONF.getLastUsedDirectoryString();
        final String filepath = ApplicationMain.IMPL.displaySaveFileDialog(startingPath);
        final JSONObject result = new JSONObject();
        result.put("filepath", filepath);
        return Response.ok().entity(result.toString(4)).build();
    }

    @POST
    @Path("/settings/save")
    @Consumes("application/json; charset=utf-8")
    @Produces("application/json; charset=utf-8")
    public Response saveSettings(final String settings) {
        final JSONObject jsn = new JSONObject(settings);

        ApplicationMain.CONF.setAutoReportOption(jsn.getBoolean("autoReport"));
        ApplicationMain.CONF.setAutoUpdateOption(jsn.getBoolean("autoUpdate"));
        ApplicationMain.CONF.setConfiguredLanguage(jsn.getString("defaultLang"));
        ApplicationMain.CONF.setDefaultBasarNumber(jsn.getString("defaultBasarNumber"));
        ApplicationMain.CONF.setDefaultFirstName(jsn.getString("defaultFirstName"));
        ApplicationMain.CONF.setDefaultLastName(jsn.getString("defaultLastName"));
        ApplicationMain.CONF.setEnableDebug(jsn.getBoolean("enableDebug"));
        ApplicationMain.CONF.setServicePort(jsn.getInt("servicePort"));

        return Response.ok(jsn.toString(4)).build();
    }

    @GET
    @Path("/settings/get")
    @Produces("application/json; charset=utf-8")
    public Response getSettings() {

        final JSONObject settings = new JSONObject();

        settings.put("defaultFirstName", ApplicationMain.CONF.getDefaultFirstName());
        settings.put("defaultLastName", ApplicationMain.CONF.getDefaultLastName());
        settings.put("defaultBasarNumber", ApplicationMain.CONF.getDefaultBasarNumber());
        settings.put("servicePort", ApplicationMain.CONF.getServicePort());
        settings.put("autoUpdate", ApplicationMain.CONF.getAutoUpdateOption());
        settings.put("autoReport", ApplicationMain.CONF.getAutoReportOption());
        settings.put("enableDebug", ApplicationMain.CONF.getEnableDebug());
        settings.put("defaultLang", ApplicationMain.CONF.getConfiguredLanguage());

        return Response.ok(settings.toString(4)).build();

    }

    @GET
    @Path("/quit")
    public Response endProgram(@QueryParam("force") final boolean force) {

        return Response.serverError().entity("Not yet implemented").build();
        // final JSONObject responseObject = new JSONObject();
        // if (force) {
        // ApplicationMain.IMPL.shutdownService();
        // System.exit(0);
        // } else {
        // final HashSet<UUID> dirtyOnes = new HashSet<>();
        // final Set<UUID> docIds = ListDocumentFactory.impl.getIds();
        // for (final UUID id : docIds) {
        // try {
        // if (ListDocumentFactory.impl.getListDocument(id).isDirty()) {
        // dirtyOnes.add(id);
        // }
        // } catch (final ListDocumentNotFoundException e) {
        // // this should never happen
        // }
        // }
        //
        // if (dirtyOnes.isEmpty()) {
        // ApplicationMain.IMPL.shutdownService();
        // System.exit(0);
        // } else {
        // responseObject.put("status", 300);
        // responseObject.put("msg", "Some lists are not saved");
        // responseObject.put("dirty", new JSONArray(dirtyOnes));
        // }
        // }
        //
        // return Response.ok().entity(responseObject.toString(4)).build();
    }

    @GET
    @Path("/printList/{id}")
    @Produces("application/json; charset=utf-8")
    public Response printList(@PathParam("id") final String id) {
        final UUID docId = UUID.fromString(id);
        ListDocument document;
        try {
            document = ListDocumentFactory.getFactory().getListDocument(docId);
        } catch (final ListDocumentNotFoundException e) {
            return this.generateErrorResponseObject(404, "List not found");
        }
        final DocumentConverter converter = new DocumentConverter();
        final DocumentGenerator generator = new DocumentGenerator();
        final String xml = converter.convertDocumentToXmlString(document);
        byte[] pdf;
        try {
            pdf = generator.generateListPDF(xml);
        } catch (IOException | TransformerException | SAXException | URISyntaxException e) {
            return this.generateErrorResponseObject(500, "Failed to produce document for printing");
        }
        final boolean result = ApplicationMain.IMPL.printWithDialog(pdf);
        final JSONObject response = new JSONObject();
        response.put("printed", result);
        return Response.ok().build();
    }

    @GET
    @Path("/printLabels/{id}")
    @Produces("application/json; charset=utf-8")
    public Response printLabels(@PathParam("id") final String id) {
        final UUID docId = UUID.fromString(id);
        ListDocument document;
        try {
            document = ListDocumentFactory.getFactory().getListDocument(docId);
        } catch (final ListDocumentNotFoundException e) {
            return this.generateErrorResponseObject(404, "List not found");
        }
        final DocumentConverter converter = new DocumentConverter();
        final DocumentGenerator generator = new DocumentGenerator();
        final String xml = converter.convertDocumentToXmlString(document);
        byte[] pdf;
        try {
            pdf = generator.generateLabelsPDF(xml);
        } catch (IOException | TransformerException | SAXException | URISyntaxException e) {
            return this.generateErrorResponseObject(500, "Failed to produce document for printing");
        }
        final boolean result = ApplicationMain.IMPL.printWithDialog(pdf);
        final JSONObject response = new JSONObject();
        response.put("printed", result);
        return Response.ok().build();
    }

    @GET
    @Path("/printNumber/{id}")
    @Produces("application/json; charset=utf-8")
    public Response printNumber(@PathParam("id") final String id) {
        final UUID docId = UUID.fromString(id);
        ListDocument document;
        try {
            document = ListDocumentFactory.getFactory().getListDocument(docId);
        } catch (final ListDocumentNotFoundException e) {
            return this.generateErrorResponseObject(404, "List not found");
        }
        final DocumentConverter converter = new DocumentConverter();
        final DocumentGenerator generator = new DocumentGenerator();
        final String xml = converter.convertDocumentToXmlString(document);
        byte[] pdf;
        try {
            pdf = generator.generateCustomerNumberPage(xml);// .generateListPDF(xml);
        } catch (IOException | TransformerException | SAXException | URISyntaxException e) {
            return this.generateErrorResponseObject(500, "Failed to produce document for printing");
        }
        final boolean result = ApplicationMain.IMPL.printWithDialog(pdf);
        final JSONObject response = new JSONObject();
        response.put("printed", result);
        return Response.ok().build();
    }

    @GET
    @Path("/printEmptyList")
    @Produces("application/json; charset=utf-8")
    public Response printEmpty(@PathParam("id") final String id) {
        final UUID docId = UUID.fromString(id);
        ListDocument document;
        ListDocument emptyDoc;
        try {
            document = ListDocumentFactory.getFactory().getListDocument(docId);
            emptyDoc = (ListDocument) document.clone();
            emptyDoc.getBody().getItems().clear();
            for (int i = 1; i <= 50; i++) {
                final ListItem emptyItem = new ListItem();
                emptyItem.setNumber(i);
                emptyItem.setDescription("");
                emptyItem.setPrice("");
                emptyItem.setSize("");
                try {
                    emptyDoc.getBody().addItem(emptyItem);
                } catch (final ItemAlreadyExistsException e) {
                    // this should not happen
                }
            }
            final DocumentConverter converter = new DocumentConverter();
            final DocumentGenerator generator = new DocumentGenerator();
            final String xml = converter.convertDocumentToXmlString(emptyDoc);
            byte[] pdf;
            try {
                pdf = generator.generateListPDF(xml);
            } catch (IOException | TransformerException | SAXException | URISyntaxException e) {
                return this.generateErrorResponseObject(500, "Failed to produce document for printing");
            }
            final boolean result = ApplicationMain.IMPL.printWithDialog(pdf);
            final JSONObject response = new JSONObject();
            response.put("printed", result);
            return Response.ok().build();
        } catch (final ListDocumentNotFoundException e) {
            return this.generateErrorResponseObject(404, "List not found");
        }

    }

    @GET
    @Path("/printAll/{id}")
    @Produces("application/json; charset=utf-8")
    public Response printAll(@PathParam("id") final String id) {
        final UUID docId = UUID.fromString(id);
        ListDocument document;
        try {
            document = ListDocumentFactory.getFactory().getListDocument(docId);
        } catch (final ListDocumentNotFoundException e) {
            return this.generateErrorResponseObject(404, "List not found");
        }
        final DocumentConverter converter = new DocumentConverter();
        final DocumentGenerator generator = new DocumentGenerator();
        final String xml = converter.convertDocumentToXmlString(document);
        byte[] labelPdf;
        byte[] listPdf;
        byte[] merged;
        try {
            listPdf = generator.generateListPDF(xml);
            labelPdf = generator.generateLabelsPDF(xml);
            merged = generator.mergePdf(listPdf, labelPdf);
        } catch (IOException | TransformerException | SAXException | URISyntaxException e) {
            return this.generateErrorResponseObject(500, "Failed to produce document for printing");
        }
        final boolean result = ApplicationMain.IMPL.printWithDialog(merged);
        final JSONObject response = new JSONObject();
        response.put("printed", result);
        return Response.ok().build();

    }

    @GET
    @Path("/version")
    @Produces("application/json; charset=utf-8")
    public Response getVersion() {
        final String version = ApplicationMain.CONF.getApplicationVersion();
        final JSONObject response = new JSONObject();
        response.put("version", version);
        return Response.ok(response.toString(4)).build();
    }

    private Response generateErrorResponseObject(final int status, final String msg) {
        final JSONObject object = new JSONObject();
        object.put("status", status);
        object.put("msg", msg);
        return Response.serverError().status(status).entity(object.toString(4)).build();

    }

}
