/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.engine;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.print.PrintService;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author Jason Reed
 *
 */
public class FileIO {

    public static void saveDataToFile(final byte[] data, final File file) throws IOException {
        final Path path = Paths.get(file.getAbsolutePath());
        BufferedWriter writer = null;
        try {
            writer = Files.newBufferedWriter(path, Charset.forName("UTF-8"));
            writer.write(new String(data, Charset.forName("UTF-8")));
        } finally {
            writer.close();
        }
    }

    public static void saveDataToFileWithoutUTF8(final byte[] data, final File file) throws IOException {
        final Path path = Paths.get(file.getAbsolutePath());
        Files.write(path, data);
    }

    public static String getDataFromFile(final File file) throws IOException {
        final Path path = Paths.get(file.getAbsolutePath());
        final List<String> data = Files.readAllLines(path, Charset.forName("UTF-8"));
        final StringBuffer buffer = new StringBuffer();
        data.forEach(s -> {
            buffer.append(s);
        });
        return buffer.toString();
    }

    public static void printPDF(final byte[] pdf, final PrinterJob job, final PrintService printer) throws InvalidPasswordException, IOException, PrinterException {

        PDDocument document = null;
        try {
            document = PDDocument.load(pdf);
            job.setPageable(new PDFPageable(document));
            job.setPrintService(printer);
            job.print();
        } finally {
            document.close();
        }

    }

}
