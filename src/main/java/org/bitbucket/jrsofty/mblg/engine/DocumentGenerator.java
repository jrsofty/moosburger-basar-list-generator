/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.engine;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.DefaultConfigurationBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FopFactoryBuilder;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.bitbucket.jrsofty.mblg.util.ResourceLoader;
import org.xml.sax.SAXException;

/**
 *
 * @author Jason Reed
 *
 */
public class DocumentGenerator {

    public byte[] generateListPDF(final String xml) throws IOException, TransformerException, SAXException, URISyntaxException {
        final byte[] xsltData = this.getFileData("templates/lists.xslt");
        final byte[] xmlData = xml.getBytes(Charset.forName("UTF-8"));
        return this.pdfGeneration(xsltData, xmlData);
    }

    public byte[] generateLabelsPDF(final String xml) throws IOException, TransformerException, SAXException, URISyntaxException {

        final byte[] xsltData = this.getFileData("templates/labels.xslt");
        final byte[] xmlData = xml.getBytes(Charset.forName("UTF-8"));
        return this.pdfGeneration(xsltData, xmlData);
    }

    public byte[] generateCustomerNumberPage(final String xml) throws IOException, TransformerException, SAXException, URISyntaxException {
        final byte[] xsltData = this.getFileData("templates/customerNumber.xslt");
        final byte[] xmlData = xml.getBytes(Charset.forName("UTF-8"));
        return this.pdfGeneration(xsltData, xmlData);
    }

    public byte[] mergePdf(final byte[] pdf1, final byte[] pdf2) throws IOException {
        final PDFMergerUtility pdfMerge = new PDFMergerUtility();
        final ByteArrayInputStream document1 = new ByteArrayInputStream(pdf1);
        final ByteArrayInputStream document2 = new ByteArrayInputStream(pdf2);
        final ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] output = null;
        try {
            pdfMerge.setDestinationStream(result);
            pdfMerge.addSource(document1);
            pdfMerge.addSource(document2);

            pdfMerge.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());

            output = result.toByteArray();
        } finally {
            result.close();
        }
        return output;
    }

    private byte[] pdfGeneration(final byte[] xslt, final byte[] xml) throws TransformerException, IOException, SAXException, URISyntaxException {
        final byte[] fo = this.transformXsltToFo(xslt, xml);
        final byte[] pdf = this.transformFoToPdf(fo);
        return pdf;
    }

    private byte[] transformXsltToFo(final byte[] xsltData, final byte[] xmlData) throws TransformerException, IOException {
        final TransformerFactory tFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",
                null);
        final Source xslt = new StreamSource(new ByteArrayInputStream(xsltData));
        final Source xml = new StreamSource(new ByteArrayInputStream(xmlData));
        final Transformer transformer = tFactory.newTransformer(xslt);
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final Result fo = new StreamResult(baos);
        transformer.transform(xml, fo);
        final byte[] foOutput = baos.toByteArray();
        baos.close();
        return foOutput;
    }

    private byte[] transformFoToPdf(final byte[] foData) throws MalformedURLException, SAXException, IOException, URISyntaxException, TransformerException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            byte[] pdfOut = null;

            final DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
            Configuration cfg = null;
            try {
                cfg = cfgBuilder.build(ResourceLoader.getFileAsStream("fop.xconf.xml"));
            } catch (final Exception e) {
                e.printStackTrace();
            }
            final FopFactoryBuilder factoryBuilder = new FopFactoryBuilder(URI.create("/")).setConfiguration(cfg);
            final FopFactory fopFactory = factoryBuilder.build();

            final Fop fop = fopFactory.newFop(org.apache.xmlgraphics.util.MimeConstants.MIME_PDF, outputStream);
            final TransformerFactory factory = TransformerFactory.newInstance();
            final Transformer transformer = factory.newTransformer();
            final Source src = new StreamSource(new ByteArrayInputStream(foData));
            final Result res = new SAXResult(fop.getDefaultHandler());
            transformer.transform(src, res);
            pdfOut = outputStream.toByteArray();
            return pdfOut;
        } finally {
            outputStream.close();
        }

    }

    private byte[] getFileData(final String filepath) throws IOException {
        final InputStream fileStream = ResourceLoader.getFileAsStream(filepath);
        final byte[] data = IOUtils.toByteArray(fileStream);
        return data;
    }

}
