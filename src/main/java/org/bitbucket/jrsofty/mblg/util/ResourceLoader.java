/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.util;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;

/**
 *
 * @author Jason Reed
 *
 */
public class ResourceLoader {
    public final static String ICON_NEW_ITEM = "add.png";
    public final static String ICON_DELETE_ITEM = "delete.png";
    public final static String ICON_SAVE = "diskette.png";
    public final static String ICON_PDF = "file_extension_pdf.png";
    public final static String ICON_SAVE_AS = "file_save_as.png";
    public final static String ICON_HELP = "help.png";
    public final static String ICON_PRINT_LABELS = "labels_page.png";
    public final static String ICON_NEW = "page.png";
    public final static String ICON_EDIT_ITEM = "pencil.png";
    public final static String ICON_PRINT_ALL = "printer.png";
    public final static String ICON_PRINT_LIST = "text_list_bullets.png";
    public final static String ICON_ACCEPT = "accept_button.png";
    public final static String ICON_GEARS = "gear_in.png";

    public static Image getIconImage(final String name) throws IOException {
        final ClassLoader loader = ResourceLoader.class.getClassLoader();
        final String fullPath = "icons/" + name;
        final InputStream stream = loader.getResourceAsStream(fullPath);
        final Image image = ImageIO.read(stream);
        return image;
    }

    public static InputStream getFileAsStream(final String filename) {
        final ClassLoader loader = ResourceLoader.class.getClassLoader();
        final InputStream stream = loader.getResourceAsStream(filename);
        return stream;
    }

    public static String getTextContents(final String filename) throws IOException {
        final ClassLoader loader = ResourceLoader.class.getClassLoader();
        final InputStream stream = loader.getResourceAsStream("text/" + filename);
        final byte[] content = IOUtils.toByteArray(stream);
        return new String(content, Charset.forName("UTF-8"));
    }

    public static String getSiteContents(final String filename) throws IOException {
        final ClassLoader loader = ResourceLoader.class.getClassLoader();
        final InputStream stream = loader.getResourceAsStream("site/" + filename);
        final byte[] content = IOUtils.toByteArray(stream);
        return new String(content, Charset.forName("UTF-8"));
    }

    public static String getWebDirectoryAsExternal(final String directory) {
        final ClassLoader loader = ResourceLoader.class.getClassLoader();
        final String webDir = loader.getResource(directory).toExternalForm();
        return webDir;
    }

    public static ClassLoader getDefaultClassLoader() {
        return ResourceLoader.class.getClassLoader();
    }

}
