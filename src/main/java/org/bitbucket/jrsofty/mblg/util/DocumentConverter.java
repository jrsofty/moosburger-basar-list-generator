/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.util;

import org.bitbucket.jrsofty.mblg.data.DocumentBody;
import org.bitbucket.jrsofty.mblg.data.DocumentHeader;
import org.bitbucket.jrsofty.mblg.data.ListDocument;
import org.bitbucket.jrsofty.mblg.data.ListItem;
import org.bitbucket.jrsofty.mblg.data.ListType;
import org.bitbucket.jrsofty.mblg.exceptions.InvalidXmlException;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author Jason Reed
 *
 */
public class DocumentConverter {
    private final XStream xstream = new XStream();

    public DocumentConverter() {
        final Class<?>[] classes = new Class[] { ListDocument.class, DocumentBody.class, DocumentHeader.class, ListItem.class, ListType.class };
        XStream.setupDefaultSecurity(this.xstream);
        this.xstream.allowTypes(classes);

        this.xstream.processAnnotations(ListDocument.class);
        this.xstream.processAnnotations(ListItem.class);

    }

    public String convertDocumentToXmlString(final ListDocument document) {
        return this.xstream.toXML(document);
    }

    public ListDocument convertXmlStringToDocument(final String xml) throws InvalidXmlException {
        final Object object = this.xstream.fromXML(xml);
        if (!(object instanceof ListDocument)) {
            throw new InvalidXmlException(xml);
        }
        return (ListDocument) object;
    }
}
