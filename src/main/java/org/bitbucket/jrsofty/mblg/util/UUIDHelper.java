package org.bitbucket.jrsofty.mblg.util;

import java.util.UUID;

public class UUIDHelper {

    public UUID fromMalformedString(final String id) {
        final String first = id.substring(0, 8);
        final String second = id.substring(8, 12);
        final String third = id.substring(12, 16);
        final String fourth = id.substring(16, 20);
        final String fifth = id.substring(20, 32);

        final StringBuffer buffer = new StringBuffer();
        buffer.append(first).append("-");
        buffer.append(second).append("-");
        buffer.append(third).append("-");
        buffer.append(fourth).append("-");
        buffer.append(fifth);

        final String newid = buffer.toString();
        if (newid.length() != 36) {
            throw new RuntimeException("Faild to parse value to correct uuid string " + id + " " + newid);
        }

        return UUID.fromString(newid);

    }

}
