/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Properties;

/**
 *
 * @author Jason Reed
 *
 */
public class I18NLoader {
    public static String LANG_GERMAN = "de";
    public static String LANG_ENGLISH = "en";

    private static HashMap<String, I18NLoader> CACHED_LANGS = new HashMap<String, I18NLoader>();

    public static I18NLoader getLanguageData(final String language) throws IOException {
        if (I18NLoader.CACHED_LANGS.containsKey(language)) {
            return I18NLoader.CACHED_LANGS.get(language);
        }
        final I18NLoader loader = new I18NLoader(language);
        I18NLoader.CACHED_LANGS.put(language, loader);
        return loader;
    }

    private final Properties languageProperties = new Properties();

    private I18NLoader(final String language) throws IOException {
        this.loadLanguageFile(language);
    }

    public String getTextByKey(final String key) {
        String result = "[[" + key + "]]";
        if (this.languageProperties.containsKey(key)) {
            result = this.languageProperties.getProperty(key);
        }
        return result;
    }

    private void loadLanguageFile(final String language) throws IOException {
        final String filename = "lang/i18n-" + language.toLowerCase() + ".lang";
        final InputStreamReader reader = new InputStreamReader(ResourceLoader.getFileAsStream(filename), Charset.forName("UTF-8"));
        this.languageProperties.load(reader);
    }

}
