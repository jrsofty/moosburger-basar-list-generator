/*******************************************************************************
 * Copyright (C) 2018 Jason Reed
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package org.bitbucket.jrsofty.mblg.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author Jason Reed
 *
 */
public class AppConf {

    private static String MBLG_CONF_LANG_KEY = "app.lang.default";
    private static String MBLG_CONF_AUTOUPDATE = "option.autoupdate";
    private static String MBLG_CONF_AUTOREPORT = "option.autoreport";
    private static String MBLG_CONF_ENABLEDEBUG = "option.enabledebug";
    private static String MBLG_LAST_USED_DIR = "last.used.dir";
    private static String MBLG_DEFAULT_FIRSTNAME = "first.name.default";
    private static String MBLG_DEFAULT_LASTNAME = "last.name.default";
    private static String MBLG_DEFAULT_BASARNUMBER = "basar.number.default";
    private static String MBLG_VERSION = "application.version";
    private static String MBLG_SERVICE_PORT = "service.port";
    private static String MBLG_DEBUG_MODE = "debug";

    private final Properties confProperties = new Properties();
    private final String userHomePath = System.getProperty("user.home") + "/.mblg";

    public AppConf() throws IOException {
        final File dir = new File(this.userHomePath);
        final File file = new File(dir + "/mblg.conf");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        if (!file.exists()) {
            this.confProperties.load(ResourceLoader.getFileAsStream("mblg.conf"));
            this.confProperties.put(AppConf.MBLG_LAST_USED_DIR, System.getProperty("user.home") + "/Documents");
            file.createNewFile();
            final FileWriter writer = new FileWriter(file);
            this.confProperties.store(writer, "");
        } else {
            this.confProperties.load(new FileReader(file));
            this.updateConfiguration();
        }

    }

    public String getApplicationVersion() {
        return this.getConfiguration(AppConf.MBLG_VERSION);
    }

    public String getConfiguration(final String key) {
        String value = "";
        if (this.confProperties.containsKey(key)) {
            value = this.confProperties.getProperty(key);
        }
        return value;
    }

    public void addConfiguration(final String key, final String value) {
        this.confProperties.putIfAbsent(key, value);
        this.storeProperties();
    }

    public String getDefaultFirstName() {
        final String firstName = this.confProperties.getProperty(AppConf.MBLG_DEFAULT_FIRSTNAME);
        return firstName;
    }

    public void setDefaultFirstName(final String firstName) {
        this.confProperties.setProperty(AppConf.MBLG_DEFAULT_FIRSTNAME, firstName);
        this.storeProperties();
    }

    public String getDefaultLastName() {
        final String lastName = this.confProperties.getProperty(AppConf.MBLG_DEFAULT_LASTNAME);
        return lastName;
    }

    public void setDefaultLastName(final String lastName) {
        this.confProperties.setProperty(AppConf.MBLG_DEFAULT_LASTNAME, lastName);
        this.storeProperties();
    }

    public String getConfiguredLanguage() {
        final String lang = this.confProperties.getProperty(AppConf.MBLG_CONF_LANG_KEY);
        return lang;
    }

    public void setConfiguredLanguage(final String lang) {
        this.confProperties.setProperty(AppConf.MBLG_CONF_LANG_KEY, lang);
        this.storeProperties();
    }

    public boolean getAutoUpdateOption() {
        return Boolean.parseBoolean(this.confProperties.getProperty(AppConf.MBLG_CONF_AUTOUPDATE));
    }

    public void setAutoUpdateOption(final boolean value) {
        final String stringValue = Boolean.toString(value);
        this.confProperties.setProperty(AppConf.MBLG_CONF_AUTOUPDATE, stringValue);
        this.storeProperties();
    }

    public boolean getAutoReportOption() {
        return Boolean.parseBoolean(this.confProperties.getProperty(AppConf.MBLG_CONF_AUTOREPORT));
    }

    public void setAutoReportOption(final boolean value) {
        final String stringValue = Boolean.toString(value);
        this.confProperties.setProperty(AppConf.MBLG_CONF_AUTOREPORT, stringValue);
        this.storeProperties();
    }

    public void setEnableDebug(final boolean value) {
        final String stringValue = Boolean.toString(value);
        this.confProperties.setProperty(AppConf.MBLG_CONF_ENABLEDEBUG, stringValue);
        this.storeProperties();
    }

    public boolean getEnableDebug() {
        return Boolean.parseBoolean(this.confProperties.getProperty(AppConf.MBLG_CONF_ENABLEDEBUG));
    }

    public String getDefaultBasarNumber() {
        return this.confProperties.getProperty(AppConf.MBLG_DEFAULT_BASARNUMBER);
    }

    public void setDefaultBasarNumber(final String basarNumber) {
        this.confProperties.put(AppConf.MBLG_DEFAULT_BASARNUMBER, basarNumber);
        this.storeProperties();
    }

    public File getLastUsedDirectoryFile() {
        String path = this.confProperties.getProperty(AppConf.MBLG_LAST_USED_DIR);
        if ((null == path) || path.isEmpty()) {
            path = System.getProperty("user.home") + "/Documents";
        }
        final File file = new File(path);
        return file;
    }

    public String getLastUsedDirectoryString() {
        String path = this.confProperties.getProperty(AppConf.MBLG_LAST_USED_DIR);
        if ((null == path) || path.isEmpty()) {
            path = System.getProperty("user.home") + "/Documents";
        }
        return path;
    }

    public void setLastUsedDirectory(final File file) {
        String path = "";
        if (file.isFile()) {
            path = file.getAbsolutePath().substring(0, (file.getAbsolutePath().length() - file.getName().length() - 1));
        } else {
            path = file.getAbsolutePath();
        }
        this.confProperties.setProperty(AppConf.MBLG_LAST_USED_DIR, path);
        this.storeProperties();
    }

    public void setServicePort(final int port) {
        this.confProperties.setProperty(AppConf.MBLG_SERVICE_PORT, "" + port);
    }

    public int getServicePort() {
        final String portString = this.confProperties.getProperty(AppConf.MBLG_SERVICE_PORT);
        return Integer.parseInt(portString);
    }

    public boolean getDebugMode() {

        return this.confProperties.containsKey(AppConf.MBLG_DEBUG_MODE);
    }

    private void storeProperties() {
        try {
            final FileWriter writer = new FileWriter(new File(this.userHomePath + "/mblg.conf"));
            this.confProperties.store(writer, "");

        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    private void updateConfiguration() throws IOException {
        final Properties defaultProperties = new Properties();
        defaultProperties.load(ResourceLoader.getFileAsStream("mblg.conf"));
        for (final Object keyObj : defaultProperties.keySet()) {
            if (keyObj instanceof String) {
                final String key = (String) keyObj;

                this.confProperties.putIfAbsent(key, defaultProperties.get(key));
            }
        }

        final ArrayList<String> keysToRemove = new ArrayList<>();
        for (final Object keyObj : this.confProperties.keySet()) {
            if (keyObj instanceof String) {
                final String key = (String) keyObj;
                if (defaultProperties.containsKey(key) || key.equals(AppConf.MBLG_DEBUG_MODE)) {
                    continue;
                }
                keysToRemove.add(key);
            }
        }
        for (final String key : keysToRemove) {
            this.confProperties.remove(key);
        }

        this.storeProperties();

    }
}
